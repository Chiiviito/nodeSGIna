'use strict';

var $routeProviderReference;
var app = angular.module('app', ['ngRoute', 'ngResource']);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider, $state) {
	$locationProvider.html5Mode(true);
	$routeProviderReference = $routeProvider;
	//document.title = $state.current.data.title;
	
}]);

app.run(['$rootScope', '$http', '$route', function($rootScope, $http, $route, $state) {
	$rootScope.dolar = '';
	$rootScope.rutaAEntrar = '';
	$rootScope.usuario = {};
	$rootScope.username = null;
	$rootScope.permissions = [];

	$http.post('/getUserConnect').then(function(res) {
		if(res.data) {
			$rootScope.username = res.data.username;
			$rootScope.permissions = res.data.permissions;
			console.log($rootScope.permissions);
		}
	});
	/**
	 * Carga las rutas desde el backend
	 */
	$routeProviderReference.when('/', {
		templateUrl : '/views/main/index.html'
	});
	$rootScope.routes = [];

	$http.post('/sgina/get-routes').then(function(res) {
		$rootScope.routes = res.data;
		$.each(res.data, function( index, value ) {
			$routeProviderReference.when(value.route, {
				templateUrl : '/views' + value.template,
				title: value.title,
				path: value.path,
				permissionAccess: value.permissionAccess
			});
		});
		
		$routeProviderReference.otherwise({ redirectTo: '/' }); // Lugar donde redirecciona si no encuentra la ruta
		$route.reload();
	});

	function checkPermissionsRoute(currentRoute) {
		let permiso = false;
		permiso = $rootScope.hasPermission(currentRoute.$$route.permissionAccess);
		if(currentRoute.$$route.permissionAccess == 'usuario.acceso') permiso = true;
		if(currentRoute.$$route.originalPath == '/') permiso = true;
		return permiso;
	}
	function trim(s, c) {
		if (c === "]") c = "\\]";
		if (c === "\\") c = "\\\\";
		return s.replace(new RegExp(
			"^[" + c + "]+|[" + c + "]+$", "g"
		), "");
	}
	
	function getModulePath(u) {
		let l = document.createElement("a");
		l.href = u;
		let tm = trim(l.pathname, "/").split('/')[0];
		return (tm) ? tm : '/';
	}
	$rootScope.hasPermission = function(p) {
		if(!p) return false;
		return $rootScope.permissions.indexOf(p) > -1;
	}

	// Lo que sucede cuando termina de cambiar de pagina
	$rootScope.$on('$routeChangeSuccess', function(event, currentRoute, previousRoute) {
		if(!checkPermissionsRoute(currentRoute)) {
			$rootScope.rutaAEntrar = currentRoute;
			window.location.href = "/";
		}
		document.title = 'SGIna';
		$rootScope.navBarUrl = '';

		if(currentRoute.$$route.title) {
			document.title = currentRoute.$$route.title;
			$rootScope.navBarUrl = '/views/' + currentRoute.$$route.path + '/navBar.html';
		}
	});
	// Lo que sucede cuando esta por cambiar de pagina
	$rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl) {
		/*if(!checkPermissionsRoute(newUrl)) {
			window.location.href = "/";
		}*/
		//console.log($rootScope.permissions);
		if(getModulePath(newUrl) != getModulePath(oldUrl)) {
			event.preventDefault();
			window.location.href = newUrl;
		}
	});
}]);

app.controller('navBarController', function($rootScope, $scope, $http, $location, $q) {
	$rootScope.desconectarUsuario = function() {
		$http.post('/salir').then(function() {
			$location.url('/');
		});
	}
});