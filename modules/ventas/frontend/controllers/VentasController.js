app.controller('NavBarServiciosController', function($scope, Caja, $location) {

});
app.controller('VentasController', function($scope, Contacto, Producto, Venta, $location) {
    $scope.ventas = Venta.query();
    $scope.contactos = Contacto.query();
    $scope.productos = Producto.query();

    $scope.contactoPorId = function(id) {
        return $scope.contactos.find((c)=> { return c._id == id});
    }
    $scope.productoPorId = function(id) {
        return $scope.productos.find((p)=> { return p._id == id});
    }

    $scope.abrirVenta = function(id) {
        $location.path('/ventas/detalle/' + id);
    }
});
app.controller('VentasDetalleController', function($scope, Contacto, $location, Venta, Producto, $routeParams) {
    $scope.contactos = Contacto.query();
    $scope.productos = Producto.query();

    //$scope.precioLetra = numeroALetras(150000);

    if($routeParams._id) {
        $scope.venta = Venta.get({_id: $routeParams._id});
    } else {
        $scope.cuotas;
        $scope.venta = {
            fecha: moment().format("DD/MM/YYYY"),
            notas: [],
            cuotas: [],
            cuotasAlDolar: false
        };
    }

    $scope.calcularDeuda = function() {
        if($scope.venta.precio == $scope.venta.pagado || $scope.venta.pagado >= $scope.producto.precioContado) {
            $scope.venta.cuotas = [];
            $scope.venta.deuda = 0
        } else {
            $scope.venta.deuda = $scope.venta.precio - $scope.venta.pagado;
        }
    }

    $scope.armarCuotas = function() {
        if($scope.cuotas > 0) {
            $scope.venta.cuotas = [];
            var mes;
            for(var i = 0; i < $scope.cuotas; i++) {
                mes = moment().add(i, 'M').format("DD/MM/YYYY");
                $scope.venta.cuotas[i] = {
                    fecha: mes,
                    valor: Math.floor($scope.venta.deuda / $scope.cuotas),
                    estado: 'PENDIENTE', // PAGADO, ATRASADO
                    datos: {
                        dolarMomento: 27,
                        dolarHoy: 29
                    }
                };
            }
        }
    }

    $scope.aplicarPrecioProducto = function() {
        $scope.producto = $scope.productos.find((p)=> { return p._id == $scope.venta.productoId});
        $scope.venta.precio = $scope.producto.precio;
    }
    // -- Notas
    $scope.nota;
    $scope.agregarNota = function() {
        $scope.venta.notas.push({
            nota: $scope.nota,
            fecha: moment().format("DD/MM/YYYY")
        });
        $scope.nota = '';
    }
    $scope.eliminarNota = function(index) {
        $scope.venta.notas.splice(index, 1);
    }
    // --
    $scope.guardar = function() {

        var valores = ['productoId', 'contactoId', 'precio', 'pagado'];
        for(var i = 0; i < valores.length; i++) {
            if(!$scope.venta[valores[i]]) {
                alert('El campo: ' + valores[i] + ' es obligatorio');
                return;
            }
        }

        if($scope.venta._id) {
            $scope.venta.$save(function() {
                $location.path('/ventas');
            });
        } else {
            console.log($scope.venta);
            new Venta($scope.venta).$save(function(a) {
                $location.path('/ventas');
            });
        }
    }
});