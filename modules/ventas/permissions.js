module.exports = [
	{
		name: 'Acceso',
		permission: 'ventas.acceso'
	},
	{
		name: 'Editar',
		permission: 'ventas.editar'
	}
]