/**
 * 
 * 	No importa el modelo que sea, todos tienen las mismas funciones:
 * 
 * 		- GET  - /test				:: 	Trae una lista de todos los registros
 * 		- GET  - /test/:_id			::	Trae ese unico registro si es que existe
 * 		- POST - /test/:_id			::	Actualiza ese registro, dependiendo de los campos que se envie
 * 
 */
module.exports = [
	{
		name: 'Venta', // Nombre del modelo
		nameMongoDB: 'ventas', // Nombre de la coleccion en MongoDB
		schema: {
			precioDolar: Number,
			precio: Number,
			descripcion: String,
			contactoId: String,
			productoId: String,
			fecha: String,
			pagado: Number,
			deuda: Number,
			cuotasAlDolar: Boolean,
			dolarMomentoVenta: Number,
			notas: [],
			cuotas: [],
			fechaAlta: {
				type: Date,
				default: Date.now
			}
		}
	}
]