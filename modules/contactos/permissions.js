module.exports = [
	{
		name: 'Acceso',
		permission: 'contacto.acceso'
	},
	{
		name: 'Editar',
		permission: 'contacto.editar'
	},
	{
		name: 'Eliminar',
		permission: 'contacto.eliminar'
	}
]