app.controller('ContactoController', function($scope, Contacto, $location) {
	$scope.contactos = Contacto.query();

	$scope.correoOficial = function(correos) {
		if(!correos) return '';
		for(var i = 0; i < correos.length; i++) {
			if(correos[i].oficial) {
				return correos[i].valor;
			}
		}
		return '';
	}
	$scope.telefonoOficial = function(telefonos) {
		if(!telefonos) return '';
		for(var i = 0; i < telefonos.length; i++) {
			if(telefonos[i].oficial) {
				return telefonos[i].valor;
			}
		}
		return '';
	}

	$scope.abrirContacto = function(id) {
		$location.path('/contactos/detalle/' + id);
	}
});
app.controller('ContactoDetalleController', function($scope, Contacto, $routeParams, $location) {
	$scope.correo;
	$scope.telefono;

	if($routeParams._id) {
		$scope.contacto = Contacto.get({_id: $routeParams._id});
	} else {
		$scope.contacto = {
			correos: [],
			telefonos: []
		};
	}

	$scope.hacerOficial = function(index, array) {
		for(var i = 0; i < array.length; i++) {
			array[i].oficial = false;
		}
		array[index].oficial = true;
	}

	$scope.agregarElemento = function(valor, array) {
		if(!$scope[valor]) return;
		array.push({
			valor: $scope[valor],
			oficial: false
		});
		$scope[valor] = '';
	}

	$scope.eliminarElemento = function(index, array) {
		if(confirm("Esta seguro de eliminar el correo?")) {
			array.splice(index, 1);
		}
	}

	$scope.guardar = function() {
		$scope.contacto.apellidos.toUpperCase();
		if($scope.contacto._id) {
			$scope.contacto.$save(function() {
				$location.path('/contactos');
			})
		} else {
			new Contacto($scope.contacto).$save(function() {
				$location.path('/contactos');
			});
		}
	}

	$scope.eliminar = function() {
		if(confirm("Desea eliminar el contacto?")) {
			$scope.contacto.eliminado = true;
			$scope.contacto.$save(function() {
				$location.path('/contactos');
			});
		}
	}
	$scope.$watch('contacto.pais', function (newValue, oldValue, scope) {
		if(oldValue == 'Argentina') $scope.contacto.provincia = '';
	});
});