var fs = require('fs');
var obj;
module.exports = function (app, model) {
	app.post('/sgina/get-dollar', (req, res) => {
		fs.readFile('app/config.json', 'utf8', function (err, data) {
			if (err) throw err;
			try {
				obj = JSON.parse(data);
				res.json(obj);
			} catch (e) {
				res.send(e);
				console.error(e);
			}
		});
	});
	app.post('/sgina/set-dollar', (req, res) => {
		fs.readFile('app/config.json', 'utf8', function (err, data) {
			if (err) throw err;
			try {
				obj = JSON.parse(data);
				obj.dolar = req.body.dolar;
				let data2 = JSON.stringify(obj);
				
				fs.writeFileSync('app/config.json', data2, 'utf8', function (err) {
					if (err) return console.log(err);
				});

				
				res.json(obj);

			} catch (e) {
				res.send(e);
				console.error(e);
			}
		});
	});
}