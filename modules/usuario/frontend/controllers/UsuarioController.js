app.controller('UsuarioController', function ($scope, $http, $rootScope, $location) {
	$scope.ingresar = function () {
		$http.post("/login", { username: $scope.usuario.usuario, password: $scope.usuario.clave }).then(function (res) {
			if (res.data === true) {
				$rootScope.username = $scope.usuario.usuario;
				if ($scope.rutaAEntrar) {
					$location.url($scope.rutaAEntrar);
				} else {
					$location.url('/');
				}
			} else {
				alert("Datos incorrectos");
				$scope.usuario.usuario = "";
				$scope.usuario.clave = "";
			}
		});
	}
});