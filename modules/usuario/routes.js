/**
 * Todas las rutas de template o rutas de navegador empiezan con un / y termina sin nada
 */
module.exports = [
	{
		route: '/',
		title: 'Detalle contacto',
		template: '/ingresar.html',
		navBar: '/navBar.html'
	},
	{
		route: '/ingresar',
		title: 'Ingresar',
		template: '/ingresar.html',
		navBar: '/navBar.html'
	},
	{
		route: '/salir',
		title: 'Desconectando ...',
		template: '/salir.html',
		navBar: '/navBar.html'
	}
]