module.exports = [
	{
		name: 'Acceso',
		permission: 'usuario.acceso'
	},
	{
		name: 'Editar',
		permission: 'usuario.editar'
	}
]