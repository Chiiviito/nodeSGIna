module.exports = function (app, model) {

	app.post('/getUserConnect', (req, res) => {
		if (req.session.user) {
			// Obtener permisos
			req.session.user.permissions = [];
			let p = model['UsuarioPermisos'].findOne({ 'username': req.session.user.username }).exec();
			p.then(function (h) {
				req.session.user.permissions = h.permissions;
				res.json(req.session.user);
			});
		} else {
			res.end();
		}
	});

	app.post('/salir', (req, res) => {
		req.session.destroy();
		res.send(true);
	});

	app.post('/login', (req, res) => {

		if (!req.session.user) {
			model['Usuario'].findOne({ 'username': req.body.username, password: req.body.password }, null, function (err, person) {
				if (person) {

					req.session.user = {};
					req.session.user.permissions = [];
					// Obtener permisos
					let p = model['UsuarioPermisos'].findOne({ 'username': req.body.username }).exec();
					p.then(function (h) {
						req.session.user.permissions = h.permissions;
						req.session.user.username = req.body.username;
						res.send(true);
					});
				} else {
					res.send(false);
				}
			});
		} else {
			res.json({
				message: "Ya existe una session creada"
			});
		}

	});

    /*app.post('/crearUsuario', (req, res) => {

        model['Usuario'].findOne({ 'username': req.body.username }, null, function (err, person) {
            if(!person) {
                let Usuario = new model['Usuario']({ username: req.body.username, password: req.body.password });
                Usuario.save(function (err) {
                    if (err) return handleError(err);
                    res.send("Usuario creado");
                });
            } else {
                res.json({
                    error: "El usuario ya existe"
                });
            }
        });
        //req.session.data = {

        //};
    });*/
}