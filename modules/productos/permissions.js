module.exports = [
	{
		name: 'Acceso',
		permission: 'producto.acceso'
	},
	{
		name: 'Editar',
		permission: 'producto.editar'
	}
]