app.controller('ProductoController', function($scope, Producto, $location) {
	$scope.productos = Producto.query();
	$scope.abrirProducto = function(id) {
		$location.path('/productos/detalle/' + id);
	}
});
app.controller('ProductoDetalleController', function($scope, Producto, $routeParams, $location) {
	if($routeParams._id) {
		$scope.producto = Producto.get({_id: $routeParams._id});
	} else {
		$scope.producto = {
			nombre: '',
			precio: '',
			descripcion: ''
		};
	}
	$scope.guardar = function() {
		if($scope.producto._id) {
			$scope.producto.$save(function() {
				$location.path('/productos');
			})
		} else {
			new Producto($scope.producto).$save(function() {
				$location.path('/productos');
			});
		}
	}
});