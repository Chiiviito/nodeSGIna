module.exports = {
	name: 'Productos',
	nameRoute: 'productos', // Por defecto viene el nombre del modulo en minusculas y/o separados por guiones
	//dep: ['googleMaps', 'tinyMCE', ''], // Ir a ./public/components
	//depAngular: ['fullCalendar'], // Ir a ./public/components
	controllers: [
		'/ProductoController.js'
	]
}