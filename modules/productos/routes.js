/**
 * Todas las rutas de template o rutas de navegador empiezan con un / y termina sin nada
 */
module.exports = [
	{
		route: '/detalle/:_id',
		title: 'Detalle contacto',
		template: '/detalle.html',
		navBar: '/navBar.html'
	},
	{
		route: '/nuevo',
		title: 'Nuevo contacto',
		template: '/detalle.html',
		navBar: '/navBar.html'
	},
	{
		route: '/',
		title: 'Contactos',
		template: '/index.html',
		navBar: '/navBar.html'
	}
]