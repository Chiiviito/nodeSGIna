app.controller('AdminController', function($scope, $rootScope, Usuario, UsuarioPermisos, $location, $http) {
	$scope.usuarios = Usuario.query();

	$rootScope.cambiarDolar = function(dolar) {
		$http.post('/sgina/set-dollar', {dolar:dolar}).then(function(res) {
			console.log("RES", res.data);
			alert("Dolar cambiado correctamente: " + dolar);
		});
	}

	$http.post('/sgina/get-dollar').then(function(res) {
		console.log(res.data.dolar);
		$rootScope.dolar = res.data.dolar;
	});
});
app.controller('AdminPermisosController', function($scope, Usuario, UsuarioPermisos, $http) {
	$scope.filtro = {};
	$scope.usuarios = Usuario.query();
	$scope.permisosDB = UsuarioPermisos.query() || [];
	$scope.permisos = [];
	$http.post('/sgina/get-permissions').then(function(res) {
		$scope.permisos = res.data;
	});

	$scope.aplicarPermiso = function(username, permission) {
		let encUsuario = false;
		let encPermiso = false;
		for (let i = 0; i < $scope.permisosDB.length; i++) {
			if(username == $scope.permisosDB[i].username) {
				encUsuario = true;
				console.log("Encontro al usuario");
				if($scope.permisosDB[i].permissions.length > 0) {
					console.log("Tiene permisos");				
					for (let p = 0; p < $scope.permisosDB[i].permissions.length; p++) {
						console.log("Lee permisos");						
						if($scope.permisosDB[i].permissions[p] == permission) {
							encPermiso = true;
							// Se elimina el permiso
							console.log("Elimino el permiso: " + permission);
							$scope.permisosDB[i].permissions.splice(p, 1);
							$scope.permisosDB[i].$save();
							break;
						}
					}
					if(!encPermiso) {
						// Se crea el permiso
						console.log("Agrego el permiso: " + permission);
						console.log(permission);
						$scope.permisosDB[i].permissions.push(permission);
						$scope.permisosDB[i].$save();
						break;
					}				
				} else {
					// Se crea el permiso
					console.log("Agrego el permiso: " + permission + " en length == 0");
					$scope.permisosDB[i].permissions.push(permission);
					$scope.permisosDB[i].$save();
					break;
				}
			}
			if(encUsuario) break;
		}

		if(!encUsuario) {
			let p = new UsuarioPermisos({
				username: username,
				permissions: [permission]
			});
			p.$save(function() {
				$scope.permisosDB.push(p);
				console.log("Permisos DB", $scope.permisosDB);
			});
		}
	}

	$scope.obtenerClasebtn = function(username, permission) {
		let encontro = false;
		let btn = 'btn-light';
		for (let i = 0; i < $scope.permisosDB.length; i++) {
			if(username == $scope.permisosDB[i].username) {
				if($scope.permisosDB[i].permissions.length > 0) {
					for (let p = 0; p < $scope.permisosDB[i].permissions.length; p++) {
						if($scope.permisosDB[i].permissions[p] == permission) {
							btn = 'btn-primary';
							break;
						}
					}				
				}
			}
		}
		return btn;
	}
});
app.controller('AdminUsuarioController', function($scope, Usuario, Contacto) {
	$scope.usuarios = Usuario.query();
	$scope.contactos = Contacto.query();

	$scope.usuario = {};
	$scope.abrirModal = function(usuario) {
		if(usuario)
			$scope.usuario = usuario;
		else
			$scope.usuario = {};
		$('#modalUsuario').modal('show');
	}
	$scope.cerrarModal = function() {
		$scope.usuario = {};
		$('#modalUsuario').modal('hide');
	}
	$scope.guardarContacto = function() {
		if($scope.usuario._id) {
			$scope.usuario.$save(function() {
				$('#modalUsuario').modal('hide');
			});
		} else {
			let usuario = new Usuario($scope.usuario);
			usuario.$save(function(r) {
				$scope.usuarios.push(r);
				$('#modalUsuario').modal('hide');
			});
		}
		$scope.usuario = {};
	}
	$scope.contactoPorId = function(id) {
		for (let index = 0; index < $scope.contactos.length; index++) {
			if($scope.contactos[index]._id == id) return $scope.contactos[index].apellidos + ', ' + $scope.contactos[index].nombre;
		}
		return '-';
	}
});