module.exports = {
	name: 'Administracion',
	nameRoute: 'administracion', // Nombre de la carpeta, se usa en el backend
	icon: 'icon-shield',
	controllers: [
		'/AdminController.js'
	],
	require: [
		/*
			Los require tienen que ser: 'components', 'modules', 'directive'
		*/
		"public/components/numeroaletras/numeroaletras.js",
		"modules/ventas/frontend/controllers/VentasController.js",
	]
}