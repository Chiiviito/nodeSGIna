/**
 * 
 * 	No importa el modelo que sea, todos tienen las mismas funciones:
 * 
 * 		- GET  - /test				:: 	Trae una lista de todos los registros
 * 		- GET  - /test/:_id			::	Trae ese unico registro si es que existe
 * 		- POST - /test/:_id			::	Actualiza ese registro, dependiendo de los campos que se envie
 * 
 */
module.exports = []