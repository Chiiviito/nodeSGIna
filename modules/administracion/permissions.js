module.exports = [
	{
		name: 'Acceso',
		permission: 'admin.acceso'
	},
	{
		name: 'Usuarios',
		permission: 'admin.crearUsuario'
	},
	{
		name: 'Permisos',
		permission: 'admin.permisos'
	},
	{
		name: 'GOD',
		permission: 'admin.GOD'
	}
]