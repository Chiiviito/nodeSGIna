app.controller('CajaController', function($scope, Caja, $location) {
    $scope.total = 0;
    $scope.movimientos = Caja.query(function(movimientos) {
        for(var i = 0; i < movimientos.length; i++) {
            if(movimientos[i].tipo == 1) {
                $scope.total += movimientos[i].monto;
            }
            if(movimientos[i].tipo == 0) {
                $scope.total -= movimientos[i].monto;
            }
        }
    });
    $scope.abrirDetalle = function(id) {
        $location.path('/caja/detalle/' + id);   
    }
});
app.controller('CajaDetalleController', function($scope, Caja, $routeParams, $location) {
	if($routeParams._id) {
        $scope.movimiento = Caja.get({_id: $routeParams._id});
    } else {
        $scope.movimiento = {
            fecha: moment().format("DD/MM/YYYY"),
            tipo: -1
        }
    }

    $scope.guardar = function() {
        if(!$scope.movimiento.fecha || !$scope.movimiento.descripcion || ($scope.movimiento.tipo == -1) || !$scope.movimiento.monto) {
            alert('Existen campos vacios');
            return;
        }
        if($scope.movimiento._id) {
            $scope.movimiento.$save(function() {
                $location.path('/caja');
            });
        } else {
            new Caja($scope.movimiento).$save(function() {
                $location.path('/caja');
            });
        }
    }
});