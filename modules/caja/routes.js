/**
 * Todas las rutas de template o rutas de navegador empiezan con un / y termina sin nada
 */
module.exports = [
	{
		route: '/',
		title: 'Caja',
		template: '/index.html',
		navBar: '/navBar.html'
	},
	{
		route: '/nuevo',
		title: 'Nuevo movimiento',
		template: '/detalle.html',
		navBar: '/navBar.html'
	},
	{
		route: '/detalle/:_id',
		title: 'Detalle de movimiento',
		template: '/detalle.html',
		navBar: '/navBar.html'
	}
]