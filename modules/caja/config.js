module.exports = {
	name: 'Caja',
	nameRoute: 'caja', // Por defecto viene el nombre del modulo en minusculas y/o separados por guiones
	//dep: ['googleMaps', 'tinyMCE', ''], // Ir a ./public/components
	//depAngular: ['fullCalendar'], // Ir a ./public/components
	icon: 'icon-home',
	controllers: [
		'/CajaController.js'
	]
}