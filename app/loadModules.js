const fs = require('fs');
const express = require("express");
const path = require('path');
const slugify = require('slugify');


/**
 * Se comento y se deshabilito la parte de deverificacion de permisos desde el backend
 */

module.exports = function(app, model, call) {
    //let per = [];
    let lc = [];
    let permissions = {};
    /**
     * Start modules load
     */
    //let dirPrivates = [];

    fs.readdirSync('./modules').forEach(file => {        
        // Lee el load.js para luego verificar si existe ese controller o controllerGlobal en la carperta del modulo
        if(fs.existsSync(path.join(__dirname, '/../modules/' + file + '/config.js'))) {
            let cntrls = require(path.join(__dirname, '/../modules/' + file + '/config.js'));
            let permisos = require(path.join(__dirname, '/../modules/' + file + '/permissions.js'));
            let nombre = (!cntrls.nameRoute) ? slugify(cntrls.name) : slugify(cntrls.nameRoute);
            //let nombre = cntrls.nameRoute;
            app.use('/module/' + nombre ,express.static(path.join(__dirname, '/../modules/' + file + '/frontend')));
            app.use('/views/' + nombre ,express.static(path.join(__dirname, '/../modules/' + file + '/frontend/views')));

            // Carga todos los controller que se declaran en el config.js
            cntrls.controllers.forEach(c => {
                let nameTmp = ("modules/" + file + '/frontend/controllers/' + c).replace(/(\/\/)/g, '/');
                let valTmp = ("module/" + file + '/controllers/' + c).replace(/(\/\/)/g, '/');
                if(fs.existsSync(path.join(__dirname, '/../modules/' + file + '/frontend/controllers/' + c))) {
                    lc.push([nameTmp, valTmp]);
                }
            });

            // Permisos
            permissions[cntrls.name] = [];
            permisos.forEach(p => {
                permissions[cntrls.name].push(p);
                //console.log("Permiso", p);
            });
        }

        // Lee el BackEnd
        if(fs.existsSync(path.join(__dirname, '/../modules/' + file + '/backend/backend.js'))) {
            require(path.join(__dirname, '/../modules/' + file + '/backend/backend.js'))(app,model);
        }
    });
    console.log(':: Modules loaded');
    call(lc, permissions);
}