const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
const db = mongoose.createConnection('mongodb://127.0.0.1:27017/sgina');


/**
 * 
 *  MongoDB Cliente desconectado pero todavia esta en el package.json
 * 
 */
/*const mongo = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/sgina";
const db3 = null;

MongoClient.connect(url, function(err, db2) {
    if (err) throw err;
});*/

let models = [];
module.exports = function(app, model, call) {
    let mSchee;
    fs.readdirSync('./modules').forEach(file => {
        // Lee el load.js para luego verificar si existe ese controller o controllerGlobal en la carperta del modulo
        if(fs.existsSync(path.join(__dirname, '/../modules/' + file + '/resources.js'))) {
            let resources = require(path.join(__dirname, '/../modules/' + file + '/resources.js'));
            if(resources.length > 0) {                
                // Recorre cada resource para luego cargarlo tanto en los modelos de MONGO en el BackEnd como en las directivas de Angular
                resources.forEach(r => {
                    // Crear modelo MONGO
                    if(r.schema && models.indexOf(r.name) == -1) {
                        models.push(r.name);
                        mSchee = new mongoose.Schema(r.schema, { versionKey: false });
                        model[r.name] = db.model(r.name, mSchee, r.nameMongoDB);
                    }
                });
            }
        }
    });
    /**
     * Metodos del modelo
     */

     // Trae todos
    app.get('/api/:nombre', (req, res) => {
        if(model[req.params.nombre]) {
            model[req.params.nombre].find({}, function(err, users) {
                res.send(users);
            });
        } else {
            res.send('Modelo no encontrado');
        }
    });
    
    // Trae un registro
    app.get('/api/:nombre/:_id', (req, res) => {
        if(model[req.params.nombre]) {
            model[req.params.nombre].findById(req.params._id, function(err, users) {
                res.send(users);
            });
        } else {
            res.send('Modelo no encontrado');
        }
    });

    // Elimina un registro
    app.delete('/api/:nombre/:_id', (req, res) => {
        if(model[req.params.nombre]) {
            model[req.params.nombre].findByIdAndDelete(req.params._id, function(err, reg) {
                if(err) res.send(err); 
                res.send({});
            });
        } else {
            res.send('Modelo no encontrado');
        }
    });

    // Crea un registro
    app.post('/api/:nombre', (req, res) => {
        //console.log(req.body);
        if(model[req.params.nombre]) {
            let tmp = new model[req.params.nombre](req.body);
            tmp.save(function() {
                res.send(tmp);
            });
        } else {
            res.send('Modelo no encontrado');
        }
    });

    // Actualiza un registro
    app.post('/api/:nombre/:_id', (req, res) => {
        if(model[req.params.nombre]) {
            model[req.params.nombre].findByIdAndUpdate(req.params._id, req.body, {}, function(err, reg) {
                res.send(req.body);
            });
        } else {
            res.send('Modelo no encontrado');
        }
    });

    app.get('/sgina/getModels.js', (req, res) => {
        let s = '';
        for(let i = 0; i < models.length; i++) {
            s += `app.factory('${models[i]}',function($resource){
                        return $resource('/api/${models[i]}/:_id', {_id: '@_id'}, {
                            'get':    { method:'GET' },
                            'save':   { method:'POST' },
                            'update': { method:'POST' },
                            'query':  { method:'GET', isArray:true },
                            'delete': { method:'DELETE' }
                        });
                  });`;
        }
        res.contentType('text/javascript' );
        res.end(s);
    });

    console.log(':: Models created');
    call(db, model);
}