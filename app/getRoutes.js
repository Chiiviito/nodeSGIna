const path = require('path');
const fs = require('fs');
const slugify = require('slugify');
const clone = require('clone');

module.exports = function(app) {

    app.post('/sgina/get-routes', (req, res) => {
        let allRoutes = [];
        let tm, rt;
        fs.readdirSync('./modules').forEach(file => {
            let routes = clone(require(path.join(__dirname, '/../modules/' + file + '/routes.js')));
            let cfg = require(path.join(__dirname, '/../modules/' + file + '/config.js'));
            let permissions = require(path.join(__dirname, '/../modules/' + file + '/permissions.js'));
            let nameRoute = (!cfg.nameRoute) ? slugify(cfg.name) : slugify(cfg.nameRoute);
            let  permissionAccess='';
            for (let i = 0; i < permissions.length; i++) {
                if(permissions[i].name == 'Acceso') permissionAccess = permissions[i].permission;
            }
            if(routes.length == 0) return; 
            routes.forEach(r => {
                tm = ('/'+nameRoute+'/'+r.template).replace(/(\/\/)/g, '/');
                rt = ('/'+nameRoute+'/'+r.route).replace(/(\/\/)/g, '/');

                if(tm.substr(-1) == '/') tm = tm.slice(0, -1);
                if(rt.substr(-1) == '/') rt = rt.slice(0, -1);
                r.permissionAccess = permissionAccess;
                r.template = tm;
                r.route = rt;
                r.routeIndex = '/' + nameRoute;
                r.module = cfg.name;
                r.icon = (cfg.icon) ? cfg.icon : null;
                r.path = nameRoute;
                allRoutes.push(r);
            });            
        });
        res.json(allRoutes);
    });

    app.get('/sgina/get-routes-modules', (req, res) => {
        let allRoutes = [];
        fs.readdirSync('./modules').forEach(file => {
            if(file == 'main') return;
            let cfg = require(path.join(__dirname, '/../modules/' + file + '/config.js'));
            let permissions = require(path.join(__dirname, '/../modules/' + file + '/permissions.js'));
            let nameRoute = (!cfg.nameRoute) ? slugify(cfg.name) : slugify(cfg.nameRoute);
            

            let f = {
                name: cfg.name,
                route: '/' + nameRoute,
            };
            for (let i = 0; i < permissions.length; i++) {
                if(permissions[i].name == 'Acceso') f.permissionAccess = permissions[i].permission;
            }
            if(cfg.icon) f.icon = cfg.icon;

            allRoutes.push(f);
        });
        res.json(allRoutes);
    });
}