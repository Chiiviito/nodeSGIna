const fs = require('fs');
const express = require("express");
const path = require('path');

// Datos a retornar
module.exports = function(app, call) {
    let ac = {
        css: [],
        js: []
    };

    app.use('/component' ,express.static(path.join(__dirname, '/../public/components/')));
    // Lee el load.js para luego verificar si existe ese controller o controllerGlobal en la carperta del modulo
    // Los componentes de "load" se cargan en todos los modulos
    if(fs.existsSync(path.join(__dirname, '/../public/components/load.js'))) {
        let cmts = require(path.join(__dirname, '/../public/components/load.js'));
        // Si existe el load.js, entonces recorre su array y verifica que exista los controllers que posee
        cmts.forEach(cm => {
            if(cm.css) {
                if(Array.isArray(cm.css)) {
                    cm.css.forEach(c => {
                        ac.css.push('/component' + c);
                    });
                } else {
                    ac.css.push('/component' + cm.css);
                }
            }

            if(cm.js) {
                if(Array.isArray(cm.js)) {
                    cm.js.forEach(j => {
                        ac.js.push('/component' + j);
                    });
                } else {
                    ac.js.push('/component' + cm.js);
                }
            }
        });
    }

    console.log(':: Components loaded');
    call(ac);
}